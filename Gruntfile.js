module.exports = function(grunt) {

    "use strict";

    var fs = require('fs'), pkginfo = grunt.file.readJSON("package.json");

    grunt.initConfig({

        pkg: pkginfo,

        meta: {
            banner: "/*! <%= pkg.title %> <%= pkg.version %> | <%= pkg.homepage %> | (c) 2014 YOOtheme | MIT License */"
        },

        jshint: {
            src: {
                options: {
                    jshintrc: "src/.jshintrc"
                },
                src: ["src/js/*.js"]
            }
        },

        less: (function(){

            var lessconf = {
                "docsmin": {
                    options: { paths: ["docs/less"], cleancss: true },
                    files: { "docs/css/uikit.docs.min.css": ["docs/less/uikit.less"] }
                }
            },

            themes = [];

            //themes

            //["default", "custom"].forEach(function(f){
            ["src"].forEach(function(f){

                if(grunt.option('quick') && f=="custom") return;

                if(fs.existsSync(f)) {

                    fs.readdirSync(f).forEach(function(t){

                        var themepath = f+'/'+t,
                            //distpath  = f=="default" ? "dist/css" : themepath+"/dist";
                            distpath  =  "dist/css";

                        // Is it a directory?
                        if (fs.lstatSync(themepath).isDirectory() && t!=="blank" && t!=='.git') {

                            var files = {};

                            if(t=="default") {
                                files[distpath+"/uikit.css"] = [themepath+"/uikit.less"];
                            } else {
                                files[distpath+"/uikit."+t+".css"] = [themepath+"/uikit.less"];
                            }

                            lessconf[t] = {
                                "options": { paths: [themepath] },
                                "files": files
                            };

                            var filesmin = {};

                            if(t=="default") {
                                filesmin[distpath+"/uikit.min.css"] = [themepath+"/uikit.less"];
                            } else {
                                filesmin[distpath+"/uikit."+t+".min.css"] = [themepath+"/uikit.less"];
                            }

                            lessconf[t+"min"] = {
                                "options": { paths: [themepath], cleancss: true},
                                "files": filesmin
                            };

                            themes.push({ "path":themepath, "name":t, "dir":f });
                        }
                    });
                }
            });

            //addons

            var _path = "src/less";
          
            if(fs.existsSync(_path+'/uikit-addons.less')) {
              
                var name = 'uikit.addons',
                    dest = 'dist/css/addons';

                lessconf["addons-uikit"] = {options: { paths: ['src/less/addons'] }, files: {} };
                lessconf["addons-uikit"].files[dest+"/"+name+".css"] = [_path+'/uikit-addons.less'];

                lessconf["addons-min-uikit"] = {options: { paths: ['src/less/addons'], cleancss: true }, files: {} };
                lessconf["addons-min-uikit"].files[dest+"/"+name+".min.css"] = [_path+'/uikit-addons.less'];
            }
            

            return lessconf;
        })(),

        clean: ["dist/assets/**/*.*", "dist/*.html"],

        copy: {
            html: {
                files: [{ expand: true, cwd: "src/", src: ["*.html"], dest: "dist/" }]
            },
            assets: {
                files: [{ expand: true, cwd: "src/assets/", src: ["**/*.*"], dest: "dist/assets/" }]
            }
        },

        validation: {
            options: {
                //reset: grunt.option('reset') || false,
                stoponerror: true,
                failHard: true,
                reset: true,
                relaxerror: ['Bad value X-UA-Compatible for attribute http-equiv on element meta.'], //ignores these errors
                doctype: 'HTML5',
                reportpath: false
            },
            files: {
                src: ['dist/*.html']
            }
        },

        concat: {
            dist: {
                options: {
                    separator: "\n\n"
                },
                src: [
                    "src/js/core.js",
                    "src/js/component.js",
                    "src/js/utility.js",
                    "src/js/touch.js",
                    "src/js/alert.js",
                    "src/js/button.js",
                    "src/js/dropdown.js",
                    "src/js/grid.js",
                    "src/js/modal.js",
                    "src/js/offcanvas.js",
                    "src/js/nav.js",
                    "src/js/tooltip.js",
                    "src/js/switcher.js",
                    "src/js/tab.js",
                    "src/js/scrollspy.js",
                    "src/js/smooth-scroll.js",
                    "src/js/toggle.js",
                ],
                dest: fs.existsSync('src/js') ? "dist/js/uikit.js" : "tmp~"
            }
        },

        usebanner: {
            dist: {
                options: {
                    position: 'top',
                    banner: "<%= meta.banner %>\n"
                },
                files: {
                    src: [ 'dist/css/**/*.css', 'dist/js/**/*.js' ]
                }
            }
        },

        uglify: {
            distmin: {
                options: {
                    //banner: "<%= meta.banner %>\n"
                },
                files: {
                    "dist/js/uikit.min.js": ["dist/js/uikit.js"]
                }
            },
            addonsmin: {
                files: (function(){

                    var files = {};

                    if(fs.existsSync('src/js/addons')) {

                        fs.readdirSync('src/js/addons').forEach(function(f){

                            if(f.match(/\.js/)) {

                                var addon = f.replace(".js", "");

                                grunt.file.copy('src/js/addons/'+f, 'dist/js/addons/'+addon+'.js');

                                files['dist/js/addons/'+addon+'.min.js'] = ['src/js/addons/'+f];

                            }
                        });

                    }

                    return files;
                })()
            },
            copy_jquery: {
                files: (function(){
                    if(fs.existsSync('src/js')) {
                        grunt.file.copy('vendor/jquery.js', 'dist/js/jquery.js');
                    }
                    return false;
                })()
            }
        },

        compress: {
            dist: {
                options: {
                    archive: ("dist/uikit-"+pkginfo.version+".zip")
                },
                files: [
                    { expand: true, cwd: "dist/", src: ["css/**/*", "js/**/*", "fonts/*"], dest: "" }
                ]
            }
        },

        watch: {
            grunt: {
                options: {
                    reload: true
                },
                files: ['Gruntfile.js']
            },
            src: {
                files: ["src/**/*.less","src/js/*.js", "src/*.html", "src/assets/**/*.*"],
                tasks: ["build"]
            }
        }

    });


    grunt.registerTask("snippet", "Create a sublime snippet for a LESS component", function(lessfile) {
    });

    grunt.registerTask("snippets", "Create sublime snippets for all LESS components", function() {
        var template = "<snippet>\n\
    <content><![CDATA[{content}]]></content>\n\
    <tabTrigger>{trigger}</tabTrigger>\n\
    <scope>text.html</scope>\n\
    <description>{description}</description>\n\
</snippet>";

        var extractSnippets = function(lessfile) {
            var less = grunt.file.read(lessfile);

            var regex = /\/\/\s*<!--\s*(.+)\s*-->\s*\n((\/\/.+\n)+)/g,
                match = null;

            while (match = regex.exec(less)) {
                var name = match[1], // i.e. uk-grid
                    name = name.replace(/^\s+|\s+$/g, ''), // trim
                    content = match[2],
                    content = content.replace(/(\n?)(\s*)\/\/ ?/g,'$1$2'), // remove comment slashes from lines
                    description = ["UIkit", name, "component"].join(" ");

                grunt.log.writeln("Generating sublime snippet: " + name);

                // place tab indices
                var i = 1; // tab index, start with 1
                content = content.replace(/class="([^"]+)"/g, 'class="${{index}:$1}"') // inside class attributes
                                .replace(/(<[^>]+>)(<\/[^>]+>)/g, '$1${index}$2') // inside empty elements
                                .replace(/\{index\}/g, function() { return i++; });

                var snippet = template.replace("{content}", content)
                                    .replace("{trigger}", "uikit")
                                    .replace("{description}", description);

                grunt.file.write('dist/snippets/'+name+'.sublime-snippet', snippet);

                // move to next match in loop
                regex.lastIndex = match.index+1;
            }
        }

        var files = grunt.file.expand("src/less/**/*.less");
        files.forEach(function(filename) {
            extractSnippets(filename);
        });
    });


    // Load grunt tasks from NPM packages
    grunt.loadNpmTasks("grunt-contrib-less");
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks("grunt-contrib-concat");
    grunt.loadNpmTasks("grunt-contrib-jshint");
    grunt.loadNpmTasks("grunt-contrib-uglify");
    grunt.loadNpmTasks("grunt-contrib-compress");
    grunt.loadNpmTasks("grunt-contrib-watch");
    grunt.loadNpmTasks("grunt-banner");
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-notify');
    grunt.loadNpmTasks('grunt-html-validation');

    // Register grunt tasks
    grunt.registerTask("build", ["jshint", "less", "concat", "clean", "copy", "uglify", "validation", "usebanner"]);
    grunt.registerTask("default", ["build", "compress"]);
};
